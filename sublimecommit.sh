#!/bin/bash

# Watch files in a GUI editors session file and auto-commit them in git

# Env file containing required variables
sessionenv="${HOME}/.sublimecommit-env.sh"

if [ -e ${sessionenv} ]; then
  source ${sessionenv}
else
  echo "Environment file for sessions not found: ${sessionenv}"
  exit
fi

# Sublime logic
function sublime()
{
  # Make sure that jq, a command-line json parser, is installed
  if [ ! -e "${sublime_sesh}" ]; then
    [ $DEBUG -gt 0 ] && echo "Sublime session file does not exist"
    return 120
  fi

  jq=`command -v jq`

  if [ "${jq}" == "" ]; then
      echo "jq command to parse json not found; install it via package manager"
      return 127
  fi

  # Get files from json
  files=`${jq} '.windows[].buffers[].file' "${sublime_sesh}"`

  OLDIFS=${IFS}
  IFS=$'\n'

  # Put the list of files in our array
  for file in ${files}; do
    file=`echo ${file} | sed -e 's/\"//g'`
    if [ ${file} != "null" ]; then
      file_arr+=(${file})
    fi
  done

  IFS=${OLDIFS}
}

# VSCode logic
function vscode()
{
  # Make sure that jq, a command-line json parser, is installed
  jq=`command -v jq`

  if [ "${jq}" == "" ]; then
      echo "jq command to parse json not found; install it via package manager"
      return 127
  fi

  # SQL statement to get data
  sql="select * from ItemTable where key = 'memento/workbench.parts.editor';"
  # Get our files from the sqlite file
  vsfiles=`echo "${sql}" | sqlite3 "${vscode_sesh}" | cut -f 2- -d "|" | \
    ${jq} '.["editorpart.state"].serializedGrid.root.data[].data.editors[].value'`
  
  OLDIFS=${IFS}
  IFS=$'\n'
  
  # Put the list of files in our array
  for vsfile in ${vsfiles}; do
    # Clean up backslashes and surrounding quotes to make it valid JSON
    vsfile=$(echo ${vsfile} | sed -e 's/\\//g' -e 's/^"//' -e 's/"$//')
    # Get our file reference out
    file=`echo ${vsfile} | ${jq} '.resourceJSON.fsPath'`
    # Remove surrounding quotes
    file=`echo ${file} | sed -e 's/\"//g'`
    if [ ${file} != "null" ] && ! [[ ${file} =~ ^Untitled-[0-9]+$ ]]; then
      file_arr+=(${file})
    fi
  done

  IFS=${OLDIFS}
}

# Loop forever while evaluating the session files
while [ 1 ]; do
  # Use an array so we can compare the list of files as we loop
  file_arr=()

  # Populate file_arr variable from the function defined in the
  # editor variable defined above
  $editor

  # DEBUG > 10, just dump the list of files discovered and exit
  if [ ${DEBUG} -gt 10 ]; then
  # Now process all the files in the list
  for f in ${!file_arr[@]}; do
    echo "${file_arr[$f]}"
  done
  exit
fi

  # If file_arr_old is populated from a previous loop, compare it
  if (( ${#file_arr_old[@]} )); then
    # Get a delta of the two arrays
    diff_arr=()
    diff_arr=(`echo ${file_arr[@]} ${file_arr_old[@]} | tr ' ' '\n' | sort | uniq -u `)
    # Loop through diff_arr and pre-commit any files we find
    for f in ${diff_arr[@]}; do
      file=`echo ${f} | sed -e 's/\"//g'`
      if [ -e "${file}" ]; then
        [ $DEBUG -gt 2 ] && echo ""
        [ $DEBUG -gt 2 ] && echo "Pre/Post edit Committing: [${file}]"
        ${addfile} "${file}" "Pre/Post edit commit of file ${f}" 1>/dev/null
      fi
    done
  fi

  # Now process all the files in the list
  for f in ${!file_arr[@]}; do
    ${addfile} "${file_arr[$f]}" "Auto commit from sublimecommit.sh" 1>/dev/null
    # Anything useful to do with a return code?
    rc=$?
  done

  # Copy our array of files to compare during the next loop
  file_arr_old=()
  file_arr_old=("${file_arr[@]}")
  
  [ $DEBUG -gt 0 ] && echo -n "."

  # Pick up any changes to our env file
  source ${sessionenv}
  
  sleep ${SLEEP}
done

exit
