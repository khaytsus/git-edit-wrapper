# sublimecommit and addfile vars
# addfile script path
export addfile="${HOME}/bin/git-edit-wrapper/addfile.sh"

# Which editor to look for files in, sublime or vscode
export editor="vscode"

# Sublime session file to get list of files from
export sublime_sesh="/${HOME}/.config/sublime-text-3/Local/Auto Save Session.sublime_session"

# VS Code session file
export vscode_sesh="${HOME}/.config/Code/User/workspaceStorage/1655391076481/state.vscdb"

# How many seconds to ignore a modified file time
export MINSECS=60

# Maximum seconds old the modified date must be be to commit file
export MAXSECS=$((MINSECS+30))

# How long to sleep between loops
export SLEEP=5

# Maximum size of file to handle in bytes
export MAXSIZE=75000

# If using selinux, make sure these are set to the correct path, otherwise
# you can set them blank to disable the selinux handling
export restorecon=/sbin/restorecon
export getenforce=/sbin/getenforce

# Warning:  If autogit=1 and no git repo is available in the current or parent
# structure we create a new one in the same path as the file.  If you want a
# more global repo, do git init on a higher path and files will be added to
# that repo instead.  Example:  cd $HOME ; git init
export autogit=1

# If you do not want to do per-save commits when using a git from remote, such
# as code checked-out from github, set this to 1.  Otherwise editing four files
# would have four commits, need to squash etc, messy.  However, you probably want
# this set to 0 in this script.
export checkremote=1

# Debug script, see below for levels, 0 is off
export DEBUG=0
