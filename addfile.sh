#!/bin/bash

# Script that takes a passed-in filename and commit message and adds it to a local
# git repo.  If one does not exist, it creates one.  It also runs restorecon if
# selinux is enabled (some editors clobber contexts).

# Example usage would be to hook into a GUI editor on load event so that if the file
# is not in git already it'll add it before editing.  If it exists, this does nothing
#  on_load  - addfile.sh $fullfilepath Pre-commit of file in on_load
#  on_close - addfile.sh $fullfilepath Post-commit of file on_close
#  Manual   - addfile.sh $fullpathpath Commit message

# Then on save or some other event in your editor, add the latest changes
#  addfile $fullfilepath These are the changes blah blah

# Env file containing required variables
sessionenv="${HOME}/.sublimecommit-env.sh"

if [ -e ${sessionenv} ]; then
  source ${sessionenv}
else
  echo "Environment file for sessions not found: ${sessionenv}"
  exit
fi

# Don't touch below here

# Get our filename and commit message from command line
file=$1
shift
commit=$*

# Test the file in various ways to see if we need to continue

# Test that we can write to this file
if [ ! -w "${file}" ]; then
  echo "Cannot write to ${file}" 1>&2 
  exit 1
fi

# Pre/Post edit commit bypasses file size test
regex="^Pre/Post edit commit of file.*"
if ! [[ $commit =~ $regex ]]; then
  # See if the file age is appropriate to commit
  fileepoch=`stat -c%Y "${file}"`
  epoch=`date +%s`
  diff=$((epoch-fileepoch))
  if [ ${diff} -lt ${MINSECS} ] || [ ${diff} -gt ${MAXSECS} ]; then
    [ $DEBUG -gt 9 ] && echo "" 1>&2 
    [ $DEBUG -gt 9 ] && echo "File is too young or too old" 1>&2 
    exit 1
  fi
fi

# See if the file is an appropriate size to add to a git repo
filesize=`stat -c%s "${file}"`
if [ ${filesize} -gt ${MAXSIZE} ]; then
  [ $DEBUG -gt 0 ] && echo -n "S" 1>&2 
  [ $DEBUG -gt 3 ] && echo "" 1>&2 
  [ $DEBUG -gt 3 ] && echo "File is too big to commit, ${filesize} > ${MAXSIZE}" 1>&2 
  exit 1
fi

[ $DEBUG -gt 0 ] && echo -n "W" 1>&2 
[ $DEBUG -gt 1 ] && echo "" 1>&2 
[ $DEBUG -gt 1 ] && echo "Committing: [${file}]" 1>&2 

dir=`dirname "$file"`
filename=`basename "$file"`

# Check to see if we should use automatic message or passed in message
function readcommit
{
  if [ "${commit}" == "" ]; then
    script=`basename $0`
    commit="Automatic ${script} script commit"
  fi

  # Redirect git output to null unless in debug mode
  if [ $DEBUG -gt 0 ]; then
    git add "$filename"
  else
    git add "$filename" &> /dev/null
  fi

  # See if we have a remote repo, if we're checking for remotes
  remoteorigin=`git config --get remote.origin.url`

  if [ $checkremote == 1 ] && [ "$remoteorigin" == "" ]; then
    git commit -q -m "$commit"
  else
    echo "Added; not committing automatically for remote git repo"
  fi

  # If not checking for remotes, just do it
  if [ $checkremote == 0 ]; then
    git commit -q -m "$commit"
  fi
}

# main()

# If using selinux, restore our contexts
if [ -f "$getenforce" ]; then
  if $getenforce | grep -q 'Enforcing\|Permissive'
  then
   $restorecon "$file"
  fi
fi

# Go to the files full path to test for a git repo
cd "$dir"

# Check again to see if we have a repo
git rev-parse --git-dir >/dev/null 2>&1
rc=$?

# Is there is a git repo here?
if [ $rc == 0 ]; then
  readcommit
else
  if [ "$autogit" == "1" ]; then
    echo "Creating new git repo in $dir"
    git init -q
    readcommit
  else
# If autogit=0, offer easy way to add file to a new repo
    echo "cd \"$dir\" && git init && git add \"$filename\" && git commit -m \"$commit\""
  fi
fi