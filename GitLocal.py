import sublime
import sublime_plugin

# Sublime Text 3 user plugin to call addfile on the current file, prompting
# for input.  Assign this to a shortcut for convenience, such as replacing save-as
# with our addfile.sh, but you can make this any shortcut combo you wish
# { "keys": ["ctrl+shift+s"], "command": "git_local" },

# To hook into onload or such and run addfile, see https://github.com/twolfson/sublime-hooks

class GitLocalCommand(sublime_plugin.WindowCommand):
    def run(self):
        self.window.show_input_panel( "Git commit message", "", self.input_done, None, self.input_cancel)        

    def input_done(self, text):
        import os
        view = sublime.Window.active_view(sublime.active_window())
        fileName = view.file_name()
        cmdCode = os.system("/usr/local/bin/addfile.sh \""+fileName+"\" "+text+"")
        if(cmdCode == 0):
            print("Success")
        else:
            print("Error: Operation failed")

    def input_change(self, text):
        print('change')

    def input_cancel(self, text):
        print('cancel')
