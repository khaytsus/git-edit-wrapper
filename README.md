git-edit-wrapper
================

A set of bash scripts that wrap around your editor of choice to automate git commits after an edit

This should work with any command line editor (uemacs, em, vi, emacs, joe, etc) and does the following..

If precommit is set to 1 and there is a git repo or autogit=1, we check in existing files into the git repo before we start editing so we have immediate change history.

Then using git if possible or falling back on md5sum the script checks to see if the file has changed at all, we then look to see if there is an available git repo to check the file into.  If not and autogit is set to 1, we create a git repo.  We then do git add and prmopt the user for a commit message.

## me
This is a wrapper script around a command line editor.  In this case it's called 'me' because the editor I've used for many years is called 'me' so I named it this out of habit so I would always use the wrapper script.

### Configuration

        editor=/path/to/your/favorite/editor
Path to your CLI editor of choice

        pager=/path/to/your/favorite/pager
Path to your preferred pager of choice, in case of a read-only file detected

        restorecon=/path/to/restorecon (if using selinux)
        getenforce/path/to/getenforce (if using selinux)
If using selinux, make sure these are set to the correct path, otherwise you can set them blank to disable the selinux handling.

        autogit=1
Set this to 1 if you want it to automatically create a repo if one does not yet exist

        precommit=1
If you want it to commit the original file before editing if a repo exists or can be created (autogit=1)

autogit and precommit are independent options, however precommit cannot check in a file before editing if autogit is not set to 1 and there isn't an existing repo.

        nobinary=1
Fail if we detect a binary file

## sublimecommit.sh

This script watches files in a GUI editors session file and auto-commit them in git using addfile.sh  

New files loaded into the editor will be pre-edit commited as soon as they are seen in the Session file but this seems to take about 30s to show up in the file, depends on the editor.  When you close the file in the editor, it will also be post-edit commited so that any remaining changes are picked up.

This script is designed to run in the background (such as when you login to your machine) and it will loop forever looking for files in a GUI editor such as Sublime or VS Code.  It will then check in those files same as the console editor wrapper script described above using the addfile.sh helper script.

## addfile.sh

Helper script to commit a file into a git repo.  If the file is in an existing remote repo and the env vars are set to checkremote it will quietly add the file into the repo without a commit message.  If checkremote is disabled, or the file is not in a remote repo.

Has a cascade of conditionals to only check git for files that match the ages defined below, is writable, and the file isn't too big.  We let git handle if the file should be added or not from there.

## sublimecommit-env.sh

Copy this example file to your homedir as .sublimecommit-env.sh and then edit it from there.  There are number of variables in this file you'll want to review and/or update that are used in both sublimecommit.sh and addfile.sh 

        addfile="/${HOME}/bin/git-edit-wrapper/addfile.sh"
Path to the addfile.sh helper script

        editor="sublime"
Which editor to look for files in, sublime or vscode.  This really calls a function in the script to do the processing for that editor, currently "sublime" and "vscode" are supported.  As far as I know, "VS Codium" should work the same.  Other editors would need a function written to put files into the array which is processed by the rest of the script.

        sublime_sesh="/${HOME}/.config/sublime-text-3/Local/Auto Save Session.sublime_session"
Sublime session file to get list of files from, this path I believe should be pretty consistent across installs.  I was originally using Sublime 3 and now I am using Sublime 4 but for me the path has not changed.  Fresh installs of Sublime 4 may use a different path.

        vscode_sesh="${HOME}/.config/Code/User/workspaceStorage/1661797987096/state.vscdb"
VS Code session file, you *will* need to update this to your path on your filesystem.  Look for the directory which is a numeric string, ignore the alphanumeric ones, those are Workspaces and probably not useful here.

        MINSECS=60
How many seconds to ignore a modified file time, ie: to not pick up quick changes.

Keep in mind this script will potentially add the file with a commit message every MINSECS if addfile.sh is not set to checkrepo or this is not a remote repo.  So if you're editing a file a lot it will create a lot of git history.  But this also gives you many snapshots of the history to review as needed.

If the file to edit is in a remote repo and checkremote in addfile is enabled the file will effectively just be added to the repo every MINSECS.  You will need to commit and push to your remote when you are ready.

        MAXSECS=$((MINSECS+30))
Maximum seconds old the modified date must be be to commit file.  Maybe not needed, but seems sane to limit how old a file can be so we're not evaluating commiting older files.

        SLEEP=5
How long to sleep between loops

        MAXSIZE=75000
Maximum size of file to handle in bytes.  You can make this huge if you don't want to limit, but for me, if it's very large it's probably not a script or config file that I want to keep commits of.

        restorecon=/sbin/restorecon
        getenforce=/sbin/getenforce
If using selinux, make sure these are set to the correct path, otherwise you can set them blank to disable the selinux handling.

        autogit=1
Warning:  If autogit=1 and no git repo is available in the current or parent structure we create a new one in the same path as the file.  If you want a more global repo, do git init on a higher path and files will be added to that repo instead.  Example:  cd $HOME ; git init

        checkremote=1
If you do not want to do per-save commits when using a git from remote, such as code checked-out from github, set this to 1.  Otherwise editing four files would have four commits, need to squash etc, messy.  However, you probably want this set to 0 in this script.

        DEBUG=0
Debug script, see script for levels, 0 is off.

## History, or why this exists

I've used microEmacs since the AmigaDOS days, which was called the "Advanced Editor".  When I moved to Linux in the late 90's I quickly found microEmacs 3.12 which operated exactly the same way it did in AmigaDOS.  Newer versions changed some of the key sequences around so I've used the same binary for years now.  Problem?  Clobbers symlinks..  Clobbers selinux..  So I wrote a wrapper to work around these issues.

Recently I decided that even though I still don't really understand git well enough (see my screwy commit log for fine examples of that) I decided that even though I have a snapshot of my hard drive four times a day I still could use more granularity and history when I'm editing things, particularly config files, scripts, code...  But then I realized I'd never do it consistently enough unless I was forced to, so I added git logic to my wrapper script.

I've since cleaned it up a little, made it a little less "uemacs-centric" (even though the original name remains; 'me') and added some configurability for if using SELinux, to automatically create a git repo, etc.

I then later started using Sublime increasingly for my editing, so I added the 'sublimecommit.sh' script to automate the same thing in GUI editors.
